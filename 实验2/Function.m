function [ dy ] = Function( t,y)

dy=zeros(15,1);
%% y(1)=seta
tl=6; tu=10; k_beta=0.5;
beta_seta=Beta(t,tl,tu);
dy(1)=k_beta*(beta_seta-1)*(y(1)-pi/2);
%% y(2)=x_1_x;y(3)=.x_1_x;y(4)=x_1_y;y(5)=.x_1_y;y(6)=x_2_x;y(7)=.x_2_x;y(8)=x_2_y;y(9)=.x_2_y,
%  y(10)=x_3_x;y(11)=.x_3_x;y(12)=x_3_y;y(13)-.x_3_y
alpha_h=10.0;h0=0.4*sqrt(5);
alpha_I=10;d0=2;

x1=[y(2);y(4)];
x2=[y(6);y(8)];
x3=[y(10);y(12)];
b1=[h0*cos(y(1))+y(15);h0*sin(y(1))];
xi=[x1 x2 x3];
%bl=[b1 b2];
for i=1:3
        % 计算AUV和领导者间的距离
        h(i)=Distance(xi(:,i),b1,2);
end
for i=1:3
    for j=1:3
        % 计算AUV间的距离
        x(i,j)=Distance(xi(:,i),xi(:,j),2);
    end
end

f_h_c=F_H_C(h,alpha_h,h0);
f_I=F_I(x,alpha_I,d0);

alpha_v=2;

dy(2)=y(3);
dy(3)=-f_h_c(1,1)/h(1)*(y(2)-b1(1))-f_I(1,2)/x(1,2)*(y(2)-y(6))-f_I(1,3)/x(1,3)*(y(2)-y(10))-alpha_v*y(3);

dy(4)=y(5);
dy(5)=-f_h_c(1,1)/h(1)*(y(4)-b1(2))-f_I(1,2)/x(1,2)*(y(4)-y(8))-f_I(1,3)/x(1,3)*(y(4)-y(12))-alpha_v*y(5);

dy(6)=y(7);
dy(7)=-f_h_c(2,1)/h(2)*(y(6)-b1(1))-f_I(2,1)/x(2,1)*(y(6)-y(2))-f_I(2,3)/x(2,3)*(y(6)-y(10))-alpha_v*y(7);

dy(8)=y(9);
dy(9)=-f_h_c(2,1)/h(2)*(y(8)-b1(2))-f_I(2,1)/x(2,1)*(y(8)-y(4))-f_I(2,3)/x(2,3)*(y(8)-y(12))-alpha_v*y(9);

dy(10)=y(11);
dy(11)=-f_h_c(3,1)/h(3)*(y(10)-b1(1))-f_I(3,1)/x(3,1)*(y(10)-y(2))-f_I(3,2)/x(3,2)*(y(10)-y(6))-alpha_v*y(11);

dy(12)=y(13);
dy(13)=-f_h_c(3,1)/h(3)*(y(12)-b1(2))-f_I(3,1)/x(3,1)*(y(12)-y(4))-f_I(3,2)/x(3,2)*(y(12)-y(8))-alpha_v*y(13);
%% y(10)=s,y(11)=r
%h1=100.0;d1=100.0;
%v_h_c=V_h_c(alpha_h,h0,h1,h);
%v_I=V_i(alpha_I,d0,d1,x(1,2));
Fai=0.5*((norm([y(3) y(5)]))+norm([y(7) y(9)])+norm([y(10) y(12)]));
v0=1.0;

%diff_Fai=-(norm([y(3);y(5)])+norm([y(7);y(9)])+norm([y(10) y(12)]));

if Fai>0.55
    h_Fai=0.0;
else
    h_Fai=0.5*v0*(1+cos(pi*Fai*2/1.1));
end

zeta=0.1;
v1=h_Fai+diff(Fai)*(zeta+1.1)/((zeta+diff(Fai)/dy(10))*(zeta-diff(Fai)));
%v1=h_Fai+diff_Fai*(zeta+1.1)/((zeta+diff_Fai/dy(14))*(zeta-diff_Fai));

if v1<=v0
    dy(14)=v1;
else
    dy(14)=v0; 
end
dy(15)=dy(14);
end
