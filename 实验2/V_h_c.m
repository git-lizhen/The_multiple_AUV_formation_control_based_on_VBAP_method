function [ V_H_C ] = V_h_c( alpha_h,h0,h1,h )
%V_H_C 返回一个矩阵
% x_i代表第i个AUV的位置，b_l代表第l个虚拟领导者的位置

n=2;                      %x的维数，即智能体的个数
m=2;                      %b的维数，即虚拟领导者的个数
for i=1:n
    for l=1:m
        if h(i,l)>0 && (h(i,l)<h1 || h(i,l)==h1)
            V_H_C(i,l)=alpha_h*(1/3*(h(i,l)^3)-h0^3*log(h(i,l))-1/3*h0^3+h0^3*log(h0));
        else
            V_H_C(i,l)=0;
        end
    end
end
end

